package com.gamesys.validator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class DateFormatValidator implements ConstraintValidator<ValidDate, String> {

    private static final String DATE_PATTERN = "^([0-9]{4})(-?)(1[0-2]|0[1-9])\\2(3[01]|0[1-9]|[12][0-9])$";

    private ValidDate date;

    @Override
    public void initialize(ValidDate constraintAnnotation) {
        this.date = constraintAnnotation;
    }

    @Override
    public boolean isValid(String date, ConstraintValidatorContext context) {
        if (!StringUtils.isEmpty(date)) {
            String passwordTrimed = date.trim();
            if (!passwordTrimed.matches(DATE_PATTERN)) {
                addConstraintViolation(context, "Invalid date format! suggested: yyyy-mm-dd");
                return false;
            }
        } else if (this.date.mandatory()) {
            addConstraintViolation(context, "Date of birth is mandatory!");
            return false;
        }
        return true;
    }

    private void addConstraintViolation(ConstraintValidatorContext context, String errMsg) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(errMsg)
                .addConstraintViolation();
    }
}
