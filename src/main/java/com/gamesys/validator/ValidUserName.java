package com.gamesys.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = UserNameValidator.class)
@Target({ ElementType.TYPE, ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidUserName {

    String message() default "Invalid userName";
    boolean mandatory() default true;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
