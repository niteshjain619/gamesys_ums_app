#!/usr/bin/env bash

echo "building ums artifacts"
mvn clean install  # generate new jar files

echo "running ums app, exposed generate port 9090"
java -jar target/ums.jar  # execution