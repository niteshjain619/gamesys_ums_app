package com.gamesys.exception;

import lombok.Getter;

@Getter
public class UserNotFoundException extends UMSException {

    public UserNotFoundException(String errMsg) {
        super("ER-1000", errMsg);
    }

    public UserNotFoundException(String errCode, String errMsg) {
        super(errCode, errMsg);
    }
}
