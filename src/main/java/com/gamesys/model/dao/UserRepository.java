package com.gamesys.model.dao;

import com.gamesys.model.entity.UserDO;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserDO, Integer> {

    int countBySsn(String ssn);

    UserDO findBySsn(String ssn);
}
