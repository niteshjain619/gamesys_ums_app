package com.gamesys.service;

import com.gamesys.model.entity.UserDO;
import com.gamesys.model.request.CreateUserRequest;

public interface IUserService {

    UserDO registerUser(CreateUserRequest request);

    UserDO getUser(String ssn);
}
