package com.gamesys.validator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class PasswordValidator implements ConstraintValidator<ValidPassword, String> {

    private static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{4,20})";

    private ValidPassword password;

    @Override
    public void initialize(ValidPassword constraintAnnotation) {
        this.password = constraintAnnotation;
    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext context) {
        if (!StringUtils.isEmpty(password)) {
            String passwordTrimed = password.trim();
            if (!passwordTrimed.matches(PASSWORD_PATTERN)) {
                addConstraintViolation(context, "(length >= 4, min one lower char, min one upper char, min one digit");
                return false;
            }
        } else if (this.password.mandatory()) {
            addConstraintViolation(context, "Password is mandatory!");
            return false;
        }
        return true;
    }

    private void addConstraintViolation(ConstraintValidatorContext context, String errMsg) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(errMsg)
                .addConstraintViolation();
    }
}
