package com.gamesys.model.request;

import com.gamesys.validator.ValidDate;
import com.gamesys.validator.ValidPassword;
import com.gamesys.validator.ValidUserName;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
@ToString(exclude={"password"}, callSuper = true)
public class CreateUserRequest extends BaseRequest {

    @ValidUserName
    String userName;

    @ValidPassword(mandatory = false)
    String password;

    @ValidDate
    String dateOfBirth;

    @NotEmpty(message = "Please provide a valid SSN!")
    String ssn;
}
