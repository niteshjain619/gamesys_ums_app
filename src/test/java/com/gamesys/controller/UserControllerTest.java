package com.gamesys.controller;

import static org.junit.Assert.assertEquals;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamesys.model.entity.UserDO;
import com.gamesys.model.request.CreateUserRequest;
import com.gamesys.model.response.CreateUserResponse;
import com.gamesys.service.UserServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

/**
 * @author nitjain2
 */
@RunWith(SpringRunner.class)
@WebMvcTest(value = UserController.class, secure = false)
public class UserControllerTest {

    public static final String DATE_OF_BIRTH = "1992-07-07";
    public static final String DUMMY_NAME = "alice";
    public static final String DUMMY_SSN = "123456";
    public static final String PASSWORD = "Dummy@123";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserServiceImpl userService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void registerUserSuccess() throws Exception {
        CreateUserRequest request = buildUserRequest();
        UserDO actualUser = new UserDO();
        actualUser.setSsn(DUMMY_SSN);
        actualUser.setUserName(DUMMY_NAME);
        Mockito.when(userService.registerUser(Mockito.any(CreateUserRequest.class)))
                .thenReturn(actualUser);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/users/register")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        CreateUserResponse expectedResponse = new CreateUserResponse("Successfully registered!",
                DUMMY_NAME, DUMMY_SSN);
        JSONAssert.assertEquals(objectMapper.writeValueAsString(expectedResponse),
                response.getContentAsString(), false);
    }

    @Test
    public void validatePasswordOnRegistration() throws Exception {
        CreateUserRequest request = buildUserRequest();
        request.setPassword("invalid_password");

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post("/users/register")
                .accept(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        assertEquals(HttpStatus.BAD_REQUEST.value(), response.getStatus());
    }

    private CreateUserRequest buildUserRequest() {
        return CreateUserRequest.builder()
                .ssn(DUMMY_SSN)
                .userName(DUMMY_NAME)
                .password(PASSWORD)
                .dateOfBirth(DATE_OF_BIRTH)
                .build();
    }
}