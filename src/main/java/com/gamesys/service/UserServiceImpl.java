package com.gamesys.service;

import com.gamesys.exception.UMSException;
import com.gamesys.model.dao.UserRepository;
import com.gamesys.model.entity.UserDO;
import com.gamesys.model.request.CreateUserRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements IUserService {

    @Autowired
    private IExclusionService exclusionService;

    @Autowired
    private UserRepository dao;

    @Override
    public UserDO registerUser(CreateUserRequest request) {

        if (!exclusionService.validate(request.getDateOfBirth(), request.getSsn())) {
            throw new UMSException("ER-1001", "Can not register as present in exclusion list!");
        }
        if (dao.countBySsn(request.getSsn()) > 0) {
            throw new UMSException("ER-1002", "User already exists!");
        }
        UserDO entity = new UserDO();
        entity.setUserName(request.getUserName());
        entity.setPassword(request.getPassword());
        entity.setDateOfBirth(request.getDateOfBirth());//yyyy-mm-dd
        entity.setSsn(request.getSsn());
        dao.save(entity);
        return entity;
    }

    @Override
    public UserDO getUser(final String ssn) {
        return dao.findBySsn(ssn);
    }
}
