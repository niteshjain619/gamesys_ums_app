package com.gamesys.controller;

import com.gamesys.exception.UserNotFoundException;
import com.gamesys.model.entity.UserDO;
import com.gamesys.model.request.CreateUserRequest;
import com.gamesys.model.response.CreateUserResponse;
import com.gamesys.service.IUserService;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private IUserService userService;

    @PostMapping(value = "/register")
    public CreateUserResponse registerUser(@Valid @RequestBody CreateUserRequest request) {
        UserDO user = userService.registerUser(request);

        CreateUserResponse response = new CreateUserResponse();
        response.setMessage("Successfully registered!");
        response.setSsn(user.getSsn());
        response.setName(user.getUserName());
        return response;
    }

    @GetMapping(value = "/{ssn}")
    public UserDO getBird(@NotEmpty @PathVariable("ssn") String ssn) throws UserNotFoundException {
        return userService.getUser(ssn);
    }
}
