package com.gamesys.controller;

import com.gamesys.exception.UMSException;
import com.gamesys.exception.UserNotFoundException;
import com.gamesys.model.response.UMSErrorResponse;
import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class UMSExceptionHandler {

    @ExceptionHandler(UMSException.class)
    public UMSErrorResponse handleException(UMSException exception) {
        log.error("UMSException: Error message is: " + exception);
        return buildErrorResponse(exception);
    }

    private UMSErrorResponse buildErrorResponse(final UMSException exception) {
        return UMSErrorResponse.builder()
                .timestamp(LocalDateTime.now())
                .status(HttpStatus.BAD_REQUEST)
                .message(exception.getMessage())
                .errorCode(exception.getErrCode())
                .build();
    }

    @ExceptionHandler(UserNotFoundException.class)
    public UMSErrorResponse handleUserNotFoundException(UserNotFoundException exception) {
        log.error("ServiceException: Error message is: " + exception.getMessage());
        return buildErrorResponse(exception);
    }

    @ExceptionHandler(RuntimeException.class)
    public UMSErrorResponse handleRuntimeException(RuntimeException exception) {
        log.error("RuntimeException: Error message is: " + exception);
        return UMSErrorResponse.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .message(exception.getMessage())
                .errorCode("ER-2000")
                .timestamp(LocalDateTime.now())
                .build();
    }

    @ExceptionHandler(javax.validation.ConstraintViolationException.class)
    protected UMSErrorResponse handleConstraintViolation(
            javax.validation.ConstraintViolationException ex) {
        UMSErrorResponse apiError = new UMSErrorResponse(HttpStatus.BAD_REQUEST);
        apiError.setMessage("Validation error");
        apiError.addValidationErrors(ex.getConstraintViolations());
        return apiError;
    }


}
