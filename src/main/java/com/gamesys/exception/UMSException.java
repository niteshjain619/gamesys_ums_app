package com.gamesys.exception;

import lombok.Getter;

@Getter
public class UMSException extends RuntimeException {

    private static final long serialVersionUID = 14534645645632L;

    private String errCode;
    private String errMsg;

    public UMSException(String errMsg) {
        super(errMsg);
        this.errMsg = errMsg;
    }

    public UMSException(String errCode, String errMsg) {
        super(errMsg);
        this.errCode = errCode;
        this.errMsg = errMsg;
    }
}