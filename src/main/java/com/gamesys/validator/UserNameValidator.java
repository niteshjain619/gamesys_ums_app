package com.gamesys.validator;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Slf4j
public class UserNameValidator implements ConstraintValidator<ValidUserName, String> {

    private ValidUserName userName;

    @Override
    public void initialize(ValidUserName constraintAnnotation) {
        this.userName = constraintAnnotation;
    }

    @Override
    public boolean isValid(String userName, ConstraintValidatorContext context) {
        if (!StringUtils.isEmpty(userName)) {
            String passwordTrimed = userName.trim();
            if (passwordTrimed.contains(" ")) {
                addConstraintViolation(context, "Spaces not allowed in userName!");
                return false;
            }
        } else if (this.userName.mandatory()) {
            addConstraintViolation(context, "UserName is mandatory!");
            return false;
        }
        return true;
    }

    private void addConstraintViolation(ConstraintValidatorContext context, String errMsg) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(errMsg)
                .addConstraintViolation();
    }
}
