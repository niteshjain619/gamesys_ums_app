package com.gamesys.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateUserResponse extends BaseResponse {

    String message;
    String name;
    String ssn;
}
