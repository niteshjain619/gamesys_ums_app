FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD /target/*.jar ums.jar
EXPOSE 9090
ENTRYPOINT ["java", "-jar","/ums.jar"]