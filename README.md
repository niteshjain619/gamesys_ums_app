#UMS (User Managment System)
##Prerequisite
* maven
* java 8
* docker (optional)

##Different ways to build and run:
* execute start.sh
* java -jar /target/ums.jar
* via docker - 
  * docker build . -t gamesys/ums
  * docker run gamesys/ums -p 9090:9090
  
##Assumption
* ssn must be unique for every user and will be used as unique key for users

#Mappings
#####Register new user
curl -X POST \
  http://localhost:9090/ums/v1/users/register \
  -H 'content-type: application/json' \
  -d '{
	"userName":"thor",
	"password":"Gamesys@123",
	"dateOfBirth":"1815-12-10",
	"ssn":"70007"
}'

#####Validate password
curl -X POST \
  http://localhost:9090/ums/v1/users/register \
  -H 'content-type: application/json' \
  -d '{
	"userName":"batman",
	"password":"abc123",
	"dateOfBirth":"1810-12-10",
	"ssn":"70008"
}'

#####Validate username
curl -X POST \
  http://localhost:9090/ums/v1/users/register \
  -H 'content-type: application/json' \
  -d '{
	"userName":"iron man",
	"password":"Ilu@3000",
	"dateOfBirth":"1810-12-10",
	"ssn":"70009"
}'

#####Validate Date
curl -X POST \
  http://localhost:9090/ums/v1/users/register \
  -H 'content-type: application/json' \
  -d '{
	"userName":"ironMan",
	"password":"Ilu@3000",
	"dateOfBirth":"181-18-18",
	"ssn":"70009"
}'

#####Exlusion list
curl -X POST \
  http://localhost:9090/ums/v1/users/register \
  -H 'content-type: application/json' \
  -d '{
	"userName":"shaktiman",
	"password":"Geeta@2020",
	"dateOfBirth":"1815-12-10",
	"ssn":"85385075"
}'

#####Get user
curl -X GET http://localhost:9090/ums/v1/users/{user_ssn}