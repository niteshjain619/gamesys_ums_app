package com.gamesys.service;

import java.util.HashSet;
import java.util.Set;
import org.springframework.stereotype.Service;

@Service
public class ExclusionServiceImpl implements IExclusionService{

    //to-do: Improvement take input from file
    private static Set<String> exclusionList = new HashSet<>();

    //ssn number is the unique identifier for the user
    static {
        // combination of date:ssn
        exclusionList.add("1815-12-10:85385075");
        exclusionList.add("1912-06-23:123456789");
        exclusionList.add("1910-06-22:987654321");
    }

    @Override
    public boolean validate(String dateOfBirth, String ssn) {
        return !exclusionList.contains(dateOfBirth+":"+ssn);
    }
}
